#! python
# -*- coding: utf-8 -*-

import os
import re

from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))


def find_version(*filepath):
    """Read file and return string version number."""
    version_file = read(*filepath)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if not version_match:
        raise RuntimeError("Unable to find version string in file: {}.".format(filepath))

    return version_match.group(1)


def read(*filepath):
    """Read file and return the text."""
    here = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(here, *filepath)) as f:
        return f.read()


long_desc = read('README.md')

classifiers = [
    'Development Status :: 4 - Beta',
    'Intended Audience :: End Users/Desktop',
    'Intended Audience :: Developers',
    'Operating System :: Microsoft :: Windows',
    'Operating System :: POSIX',
    'Programming Language :: Python']

requires = ['qtpy>=1.4',
            'pyqt5>=5.9'
            'numpy>=1.13',
            'pyqtgraph>=0.10',
            'pyaudio>=0.2']

setup(name='wavytool',
      version=find_version("wavytool", "__init__.py"),
      description='Simple GUI that acquires data from input devices, plot and export files.',
      long_description=long_desc,
      long_description_content_type='text/markdown',
      url='https://gitlab.com/dpizetta/wavytool',
      author='Daniel Cosmo Pizetta',
      author_email='daniel.pizett@usp.br',
      classifiers=classifiers,
      packages=['wavytool', 'wavytool.images'],
      package_data={'wavytool_data': ['README.md', '*.png', '*.ui']},
      entry_points={"gui_scripts": ["wavytool=wavytool.__main__:main"]},
      install_requires=requires
      )
