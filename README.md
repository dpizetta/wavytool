# Wavy Tool

Wavy Tool acquire signals from input devices, e.g microphone, plot and saves as data (.csv) or exports as image
(.png, .tif, etc). It has two plot areas, which represents the real time data from input (started when the
software runs), and the recording area that starts when clicked on Record button.

This software was developed to be a simple way to acquire data from microphone channel.
The main use is to provide an acquisition software for Ruchardt's method in Physics Laboratory at Sao Carlos
Institute of Physics - University of Sao Paulo.

![alt tag](https://cloud.githubusercontent.com/assets/5084939/7890891/5014ffee-061f-11e5-84c9-b3c77f91f123.png)

This software is free for use, but it needs be cited using the DOI/link below.

Please, if you use this software, cite us: [![DOI](https://zenodo.org/badge/31438894.svg)](https://zenodo.org/badge/latestdoi/31438894)

## Download binaries - No Python required

To be practical, now we can provide binaries for Windows and Linux. The binaries are not small (200MB) but
have everything you need. You don't need to install, just download and click to execute.

After downloading you can extract the executable. We suggest you create a folder Wavy to put the executable
and the configuration file that will be created, creating a link (icon) to access it from Desktop area.
To run it, just double click.

Please, see [releases](https://github.com/dpizetta/wavy/releases) page.

## Installing using PIP

If you want to running on Python, simple install it and its dependencies doing:

`$ pip install wavytool`

Then to use it, do:

`$ wavytool`

## First steps with Wavy Tool

### Configuration

When you run it the first time, it will let you to choose the default folder to place future data files. We suggest that you create a new folder "Wavy Data" in Documents or Desktop area for easy access. This procedure will create a file that stays with the executable called "wavy.config", to keep that information and future configurations.

### Plots

Dealing with the program is very easy. The top plot shows the real-time data from input device. The bottom plot will show recorded data. The recording area has three states:

* Recording when the line is red;
* Paused when the line is orange, and;
* Stopped when the graph is green.

### Controlling

Then we can start recording by clicking the Record button. Also, if you need, you can pause using the Pause button and start recording again by clicking on Pause once more. And finally, you can stop recording by clicking on Stop button.

### Saving data

After stop acquiring data, the buttons to save and export will be enabled. By clicking on save button you can save data from recording area to CSV file. Also, you can export an image file using the exporter. Wavy automatically suggests the name for your file when saving as `new_wavy_data_yymmddHHMMSS.ext`.

**Note:** If you zoom in the recording plot, data saved will represent just the data visible in the window. So make sure you do not lost data.

**Features on plots:** Plots are provided by PyQtGraph, and if you right click on the plot you will see some nice features including other options to export data, zoom in and out, spectral analysis, etc.

**Input device not found:** If the is no input device plugged in (or internal microphone) the program will show a message and exit at this moment, we need future improvement in this way.

## Problems and improvements

If you find any problems in this program, please let us know using the [Issues System](https://gitlab.com/dpizetta/wavytool/issues) provided by GitLab. This is the correct way to keep us informed and how we can provide information about the development for you. Thanks in advance.

## Authors

* [Daniel Cosmo Pizetta] (https://github.com/dpizetta)
* [Wesley Daflita] (https://github.com/Wa59)

## Acknowledgment

Professors

* PhD Fernando Fernandes Paiva
* PhD Valmor Roberto Mastelaro

Technicians

* Antenor Fabbri Petrilli Filho
* Cláudio Boense Bretas
* Jae Antonio de Castro Filho

## Dependencies

* Python3
* QtPy
* PyQt5 or PyQt4 or PySide (PySide2 is not supported because of PyQtGraph)
* PyQtGraph
* NumPy
* PyAudio (also PortAudio)

For Windows users, PyAudio requires more things, the better way is to download
the .exe installer from PyAudio website (that includes PortAudio) and install
it. If you are using Anaconda or Miniconda, replace the 'pip' command for 'conda'.

## Running the code

To run without installing, use

`$ python -m wavytool`

## Installing from source code

To install it, do the following in the folder that has `setup.py`

`$ pip install .`

## Installing as developer

To install it as a developer, which means install it like a library but keeping
it in the same place it is and providing the auto update if any changes are made.

`$ pip install -e .`
